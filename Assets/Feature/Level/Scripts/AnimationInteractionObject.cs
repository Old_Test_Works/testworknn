﻿using System.Collections.Generic;
using System.Linq;
using Feature.Character.Interaction;
using UnityEngine;

namespace Feature.Level
{
    public class AnimationInteractionObject : InteractionObject
    {
        [SerializeField] private ParticleSystem _particle;
        [SerializeField] private Animation _animation;
        [SerializeField] private List<MeshRenderer> _mesh;

        [SerializeField] private string _animationName;

        [SerializeField] private Material _outlineMaskMaterial;
        [SerializeField] private Material _outlineFillMaterial;

        private List<Material> _spawnedMaterials = new List<Material>();

        private void Awake()
        {
            foreach (var renderer in _mesh)
            {
                var materials = renderer.sharedMaterials.ToList();

                var mask = Instantiate(_outlineMaskMaterial);
                materials.Add(mask);

                var fill = Instantiate(_outlineFillMaterial);
                materials.Add(fill);

                _spawnedMaterials.Add(fill);
                renderer.materials = materials.ToArray();
            }

            SetReadyToInteraction(false);
        }

        public override void Interaction()
        {
            base.Interaction();
            _animation.Play(_animationName);
        }

        public override void SetReadyToInteraction(bool isReady)
        {
            SetActiveParticle(isReady);
            SetActiveOutline(isReady);
        }

        private void SetActiveParticle(bool isActive)
        {
            if(!_particle)
                return;
            
            if (isActive)
            {
                _particle.Play();
            }
            else
            {
                _particle.Stop();
            }
        }

        private void SetActiveOutline(bool isActive)
        {
            foreach (var material in _spawnedMaterials)
            {
                if (isActive)
                {
                    material.SetFloat("_OutlineWidth", 5);
                }
                else
                {
                    material.SetFloat("_OutlineWidth", 0);
                }


            }
        }
    }
}