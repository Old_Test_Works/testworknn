﻿using System;

namespace Feature.Level
{
    public class LevelButtonView : AnimationInteractionObject
    {
        public event Action OnClick;

        public override void Interaction()
        {
            base.Interaction();
            OnClick?.Invoke();
        }
    }
}