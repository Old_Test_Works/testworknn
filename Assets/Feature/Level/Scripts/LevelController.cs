﻿using System;
using UnityEngine;

namespace Feature.Level
{
    public class LevelController : MonoBehaviour
    {
        [SerializeField] private CharacterMoveController CharacterMoveController;
        
        public event Action OnRepaintLevel;
        public event Action<float> OnTimeUpdate;
        public event Action<float> OnLevelEnd;
        
        private bool _isStarted = false;

        private float _timeLevel;

        private Transform _startPosition;

        private void Update()
        {
            if (_isStarted)
            {
                _timeLevel += Time.deltaTime;
                OnTimeUpdate?.Invoke(_timeLevel);
            }
        }

        public void SetStartPosition(Transform startPosition)
        {
            _startPosition = startPosition;
        }

        [ContextMenu("StartLevel")]
        public void StartLevel()
        {
            _timeLevel = 0;
            
            if(_isStarted)
                return;
            _isStarted = true;
            RepaintLevel();
        }
        
        [ContextMenu("EndLevel")]
        public void EndLevel()
        {
            CharacterMoveController.TeleportPlayer(_startPosition);
            
            if(!_isStarted)
                return;
            OnLevelEnd?.Invoke(_timeLevel);
            _isStarted = false;
        }
        
        [ContextMenu("RepaintLevel")]
        private void RepaintLevel()
        {
            OnRepaintLevel?.Invoke();
        }
    }
}