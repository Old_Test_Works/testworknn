﻿using System;
using Feature.More;
using UnityEngine;

namespace Feature.Level
{
    public class LevelRecordUIControllers : MonoBehaviour
    {
        [Header("Inject Controllers")] [SerializeField]
        private LevelController LevelController;

        [Header("----")] 
        [SerializeField] private Transform _content;

        [SerializeField] private LevelRecordView _prefab;

        private int _lastId = 0;

        private void OnEnable()
        {
            LevelController.OnLevelEnd += LevelEnd;
        }

        private void OnDisable()
        {
            LevelController.OnLevelEnd -= LevelEnd;
        }

        private void LevelEnd(float time)
        {
            _lastId++;
            var view = Instantiate(_prefab, _content);
            view.Initialize(_lastId,TimeToString.GetText(time));
        }
    }
}