using UnityEngine;
using UnityEngine.UI;

namespace Feature.Level
{
    public class LevelRecordView : MonoBehaviour
    {
        [SerializeField] private Text _id;
        [SerializeField] private Text _time;

        public void Initialize(int id, string time)
        {
            _id.text = id.ToString();
            _time.text = time;
        }
    }
}