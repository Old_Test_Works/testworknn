﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Feature.Level
{
    public class ObstacleView: MonoBehaviour
    {
        [SerializeField] private List<GameObject> _views;


#if  UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.red;
            Gizmos.DrawCube(Vector3.zero,Vector3.one);
            Gizmos.DrawLine(Vector3.zero,Vector3.forward);
        }
#endif
        
        public int MaxId
        {
            get
            {
                if (_maxId == -1)
                {
                    _maxId = _views.Count;
                }

                return _maxId;
            }
        }

        private int _maxId = -1;
        
        public void SetActiveObstacle(int id)
        {
            foreach (var view in _views)
            {
                view.SetActive(false);
            }
            
            if (id>= _views.Count)
            {
                Debug.LogError($"Слишком большой id");
                return;
            }
            
            _views[id].SetActive(true);
        }
    }
}