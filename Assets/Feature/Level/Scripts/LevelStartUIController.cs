﻿using System;
using UnityEngine;

namespace Feature.Level
{
    public class LevelStartUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField] private LevelController LevelController;


        
#if  UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(Vector3.zero,Vector3.one);
        }
#endif

        private void Awake()
        {
            LevelController.SetStartPosition(transform);
        }
    }
}