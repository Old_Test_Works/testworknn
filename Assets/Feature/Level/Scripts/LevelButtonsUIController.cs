﻿using System;
using UnityEngine;

namespace Feature.Level
{
    public class LevelButtonsUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField] private LevelController LevelController;

        [Header("----")]
        
        [SerializeField] private LevelButtonView _startButton;
        [SerializeField] private LevelButtonView _endButton;

        private void OnEnable()
        {
            _startButton.OnClick += StartLevelClick;
            _endButton.OnClick += EndLevelClick;
        }

        private void EndLevelClick()
        {
            LevelController.EndLevel();
        }

        private void StartLevelClick()
        {
            LevelController.StartLevel();
        }
    }
}