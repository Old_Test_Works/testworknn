﻿using System;
using Feature.More;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Level
{
    public class LevelTimeUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField] private LevelController LevelController;

        [Header("----")] [SerializeField] private Text _timerText;

        private void Awake()
        {
            _timerText.text = "";
        }

        private void OnEnable()
        {
            LevelController.OnTimeUpdate += UpdateTime;
        }

        private void OnDisable()
        {
            LevelController.OnTimeUpdate -= UpdateTime;
        }

        private void UpdateTime(float time)
        {
            _timerText.text = TimeToString.GetText(time);

        }
    }
}