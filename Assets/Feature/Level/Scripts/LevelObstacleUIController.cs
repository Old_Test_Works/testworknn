﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Feature.Level
{
    public class LevelObstacleUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField] private LevelController LevelController;

        [Header("----")]
        
        [SerializeField] private List<ObstacleView> _views;

 
        private void OnEnable()
        {
            LevelController.OnRepaintLevel += RepaintLevel;
        }

        private void OnDisable()
        {
            LevelController.OnRepaintLevel -= RepaintLevel;
        }

        private void RepaintLevel()
        {
            foreach (var view in _views)
            {
                view.SetActiveObstacle(Random.Range(0,view.MaxId));
            }
        }
    }
}