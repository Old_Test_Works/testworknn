﻿using UnityEngine;

namespace Feature.More
{
    public static class TimeToString
    {
        public  static string GetText(float time)
        {
            float minutes = Mathf.FloorToInt(time / 60);  
            float seconds = Mathf.FloorToInt(time % 60);
            float milliSecond = (time%1) *1000;
            return  string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliSecond);
        }
    }
}