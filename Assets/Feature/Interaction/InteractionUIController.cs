﻿using System;
using UnityEngine;

namespace Feature.Character.Interaction
{
    public class InteractionUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField] private InteractionController InteractionController;

        [Header("----")]
        
        //[SerializeField] private Transform _camera;
        
        //private Ray _ray = new Ray();

        private InteractionObject _interactionObject;

#if  UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.green;
            Gizmos.DrawCube(Vector3.zero,Vector3.one);
        }
#endif
        
        private void OnEnable()
        {
            InteractionController.OnInteractionClick += ClickInteract;
        }

        private void OnDisable()
        {
            InteractionController.OnInteractionClick -= ClickInteract;
        }


        public void ClickInteract()
        {
            /*
             не для этого надо камеру переписывать и делать вид о первого лица
            _ray = new Ray(_camera.position, _camera.forward);
            if (Physics.Raycast(_ray, out var hit, Mathf.Infinity, 100))
            {
                var interactionObject = hit.transform.gameObject.GetComponent<InteractionObject>();
                if (interactionObject)
                {
                    interactionObject.Interaction();
                }
            }
            */
            if (_interactionObject)
            {
                _interactionObject.Interaction();
            }
        }


        private void OnTriggerEnter(Collider other)
        {
            var interactionObject = other.gameObject.GetComponent<InteractionObject>();
            _interactionObject = interactionObject;
            if (_interactionObject)
            {
                _interactionObject.SetReadyToInteraction(true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (_interactionObject)
            {
                _interactionObject.SetReadyToInteraction(false);
            }

            _interactionObject = null;
        }
    }
}