﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Feature.Character.Interaction
{
    public class InteractionController : MonoBehaviour
    {
        public event Action OnInteractionClick;
        
        
        private CharacterInputSettings _characterInputSettings;
        
        private void Awake()
        {
            //TODO все ещё не уверен на сколько правильно создавать таких несколько 
            //возможно есть смысл вынести создание в одно место, но тогда каждый раз нужно будет дописывать скрипт 
            // или сделать разные настройки для каждой системы
            _characterInputSettings = new CharacterInputSettings();
        }
        
        private void OnEnable()
        {
            _characterInputSettings.Enable();
            _characterInputSettings.Character.Interaction.performed += ClickInteraction;
        }
        

        private void OnDisable()
        {
            _characterInputSettings.Disable();
            _characterInputSettings.Character.Interaction.performed -= ClickInteraction;
        }
        
        private void ClickInteraction(InputAction.CallbackContext context)
        {
            OnInteractionClick?.Invoke();
        }
    }
}