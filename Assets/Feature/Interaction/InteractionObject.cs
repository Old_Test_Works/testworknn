﻿using UnityEngine;

namespace Feature.Character.Interaction
{
    public class InteractionObject : MonoBehaviour
    {
        public virtual void Interaction()
        {
            Debug.Log($"Interaction");
        }
        
        public virtual void SetReadyToInteraction(bool isReady)
        {
            Debug.Log($"Ready");
        }
        
        
        
    }
}