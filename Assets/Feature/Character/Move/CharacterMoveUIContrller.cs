﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Feature.Character
{
    public class CharacterMoveUIContrller : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField]
        private CharacterMoveController CharacterMoveController;
        [SerializeField]
        private CharacterCameraController CharacterCameraController;

        [Header("----")]
        [SerializeField] private NavMeshAgent _agent;

        private void OnEnable()
        {
            
            CharacterMoveController.OnTeleport += Teleport;
        }

        private void OnDisable()
        {
            CharacterMoveController.OnTeleport -= Teleport;
        }


        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var delta = CharacterCameraController.CameraDirection * CharacterMoveController.MoveVector;
            _agent.SetDestination(_agent.transform.position + delta);
        }
        
        private void Teleport(Transform obj)
        {
            //_agent.se
            _agent.enabled = false;
            _agent.transform.position = obj.position;
            _agent.enabled = true;
        }
    }
}