﻿using System;
using Feature.Character.Interaction;
using UnityEngine;

namespace Feature.Character
{
    public class CharacterAnimationUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField]
        private CharacterMoveController CharacterMoveController;
        [SerializeField] private InteractionController InteractionController;

        [Header("----")]
        [SerializeField] private Animator _animator;

        private void OnEnable()
        {
            CharacterMoveController.OnJump += Jump;
            CharacterMoveController.OnRoll += Roll;
            CharacterMoveController.OnMove += Move;
            InteractionController.OnInteractionClick += Interaction;
        }
        
        private void OnDisable()
        {
            CharacterMoveController.OnJump -= Jump;
            CharacterMoveController.OnRoll -= Roll;
            CharacterMoveController.OnMove -= Move;
            InteractionController.OnInteractionClick -= Interaction;
        }


        private void Move()
        {
            if (CharacterMoveController.MoveVector.x  !=  0 || CharacterMoveController.MoveVector.z  !=  0)
            {
                _animator.SetBool("Run", true);
            }
            else
            {
                _animator.SetBool("Run", false);
            }
        }
        
        private void Interaction()
        {
            _animator.SetTrigger("Interaction");
        }

        
        private void Jump()
        {
            _animator.SetTrigger("Jump");
        }

        private void Roll()
        {
            _animator.SetTrigger("Roll");
        }
    }
}