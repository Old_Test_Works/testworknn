using System;
using System.Collections;
using System.Collections.Generic;
using Feature.Level;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterMoveController : MonoBehaviour
{

    public event Action OnMove;
    public event Action OnJump;
    public event Action OnRoll;
    public event Action<Transform> OnTeleport;
    
    public Vector3 MoveVector => _moveVector;
    
    private CharacterInputSettings _characterInputSettings;
    
    private Vector3 _moveVector = new Vector3();


    
    private void Awake()
    {
        _characterInputSettings = new CharacterInputSettings();
        
    }

    private void OnEnable()
    {
        _characterInputSettings.Enable();
        _characterInputSettings.Character.Move.performed += Move;
        _characterInputSettings.Character.Move.canceled += Move;
        _characterInputSettings.Character.Jump.performed += Jump;
        _characterInputSettings.Character.Roll.performed += Roll;
    }

    private void OnDisable()
    {
        _characterInputSettings.Disable();
        _characterInputSettings.Character.Move.performed -= Move;
        _characterInputSettings.Character.Move.canceled -= Move;
        _characterInputSettings.Character.Jump.performed -= Jump;
        _characterInputSettings.Character.Roll.performed -= Roll;
    }

    
    
    public void TeleportPlayer(Transform obj)
    {
        OnTeleport?.Invoke(obj);
    }

    private void Move(InputAction.CallbackContext context)
    {
        var vector = context.ReadValue<Vector2>();
        _moveVector.x = vector.x;
        _moveVector.z = vector.y;
        OnMove?.Invoke();
    }

    private void Jump(InputAction.CallbackContext context)
    {
        OnJump?.Invoke();
    }

    private void Roll(InputAction.CallbackContext context)
    {
        OnRoll?.Invoke();
    }
}
