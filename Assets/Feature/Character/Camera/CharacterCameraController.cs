﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Feature.Character
{
    public class CharacterCameraController : MonoBehaviour
    {
        private CharacterInputSettings _characterInputSettings;
        
        private Quaternion _current = Quaternion.identity;
        public Quaternion CameraDirection => _current;

        private float speed = 0.01f;

        private void Awake()
        {
            _characterInputSettings = new CharacterInputSettings();
        
        }

        public void SetDirection(Quaternion cameraRotation)
        {
            _current = cameraRotation;
        }
        
        private void OnEnable()
        {
            _characterInputSettings.Enable();
            _characterInputSettings.Character.Camera.performed += Rotate;
            _characterInputSettings.Character.Camera.canceled += Rotate;
        }
        
        private void OnDisable()
        {
            _characterInputSettings.Disable();
            _characterInputSettings.Character.Camera.performed -= Rotate;
            _characterInputSettings.Character.Camera.canceled -= Rotate;
        }
        
        private void Rotate(InputAction.CallbackContext context)
        {
            var vector = context.ReadValue<Vector2>();
            _current *= Quaternion.AngleAxis(vector.x * speed, Vector3.up);
        }



    }
}