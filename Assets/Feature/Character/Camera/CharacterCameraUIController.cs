﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Character
{
    public class CharacterCameraUIController : MonoBehaviour
    {
        [Header("Inject Controllers")]
        [SerializeField]
        private CharacterCameraController CharacterCameraController;

        [Header("----")]
        [SerializeField] private Transform _camera;
        [SerializeField] private Transform _player;
        [SerializeField] private Vector3 _delta = new Vector3(10, 0, 0);
        

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            CharacterCameraController.SetDirection(_camera.rotation);
        }
        
        
        private void Update()
        {
            Repaint();
        }

        private void Repaint()
        {
            _camera.rotation = CharacterCameraController.CameraDirection;
            _camera.position = _player.position +  CharacterCameraController.CameraDirection * _delta;
        }


    }
}